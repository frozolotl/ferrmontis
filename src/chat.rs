use serde::{Deserialize, Serialize};

#[derive(Default, Debug, Serialize, Deserialize)]
pub struct Chat {
    pub text: String,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub bold: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub italic: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub underlined: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub strikethrough: Option<bool>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub obfuscated: Option<bool>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub color: Option<Formatting>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub insertion: Option<String>,

    #[serde(rename = "clickEvent")]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub click_event: Option<ClickEvent>,
    #[serde(rename = "hoverEvent")]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub hover_event: Option<HoverEvent>,

    #[serde(skip_serializing_if = "Vec::is_empty")]
    pub extra: Vec<Chat>,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum Formatting {
    Black,
    DarkGray,
    DarkBlue,
    DarkGreen,
    DarkCyan,
    DarkRed,
    DarkPurple,
    Gold,

    #[serde(rename = "gray")]
    LightGray,
    #[serde(rename = "blue")]
    LightBlue,
    #[serde(rename = "green")]
    LightGreen,
    #[serde(rename = "cyan")]
    LightCyan,
    #[serde(rename = "red")]
    LightRed,
    #[serde(rename = "light_purple")]
    LightPurple,
    #[serde(rename = "yellow")]
    LightYellow,
    White,

    Random,
    Bold,
    Strikethrough,
    Underlined,
    Italic,
    Plain,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct ClickEvent {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub open_url: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub run_command: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub suggest_command: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub change_page: Option<String>,
}

// todo: comply with the protocol
#[derive(Debug, Serialize, Deserialize)]
pub struct HoverEvent {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub show_text: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub show_item: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub show_entity: Option<String>,
}
