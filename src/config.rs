use crate::error::*;
use serde::{
    de::{self, Visitor},
    Deserialize, Deserializer, Serialize,
};
use std::{env, fmt, fs::File, io, net::SocketAddr, path::PathBuf, result::Result as SResult};

#[derive(Debug, Default, Serialize, Deserialize)]
pub struct Config {
    #[serde(default)]
    pub net: NetConfig,

    #[serde(default)]
    pub status: StatusConfig,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct NetConfig {
    pub address: SocketAddr,
}

impl Default for NetConfig {
    fn default() -> NetConfig {
        NetConfig {
            address: ([127, 0, 0, 1], 25565).into(),
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct StatusConfig {
    pub max_players: u32,
    pub sample_size: u16,
    pub description: String,

    #[serde(deserialize_with = "path_to_base64")]
    pub favicon: Option<String>,
}

impl Default for StatusConfig {
    fn default() -> StatusConfig {
        StatusConfig {
            max_players: 16,
            sample_size: 4,
            description:
                "§7This is §bferrmontis.\n§7An experimental minecraft server written in rust."
                    .into(),
            favicon: None,
        }
    }
}

/// Reads the configuration file or creates one if none was found.
/// Exits with status code 1 if the home directory cannot be found.
pub fn read_config() -> Result<Config> {
    let path = env::var("CONFIG_PATH").unwrap_or_else(|_| String::from("./ferrmontis.toml"));
    let path = PathBuf::from(path);

    match File::open(&path) {
        Ok(file) => Ok(serde_yaml::from_reader(file)?),
        Err(ref err) if err.kind() == io::ErrorKind::NotFound => {
            let file = File::create(path)?;
            let cfg = Config::default();
            serde_yaml::to_writer(file, &cfg)?;
            Ok(cfg)
        }
        Err(err) => Err(err.into()),
    }
}

fn path_to_base64<'de, D>(deserializer: D) -> SResult<Option<String>, D::Error>
where
    D: Deserializer<'de>,
{
    struct PathToBase64;

    impl<'de> Visitor<'de> for PathToBase64 {
        type Value = Option<String>;

        fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
            formatter.write_str("file path")
        }

        fn visit_str<E>(self, value: &str) -> SResult<Option<String>, E>
        where
            E: de::Error,
        {
            let content = std::fs::read(value).map_err(de::Error::custom)?;
            let mut data = String::from("data:image/png;base64,");
            base64::encode_config_buf(&content, base64::STANDARD, &mut data);
            Ok(Some(data))
        }
    }

    deserializer.deserialize_string(PathToBase64)
}
