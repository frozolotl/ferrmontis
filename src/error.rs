use std::{error, fmt, io};

pub type Result<T> = std::result::Result<T, Error>;

#[derive(Debug)]
pub enum Error {
    IO(io::Error),
    YAML(serde_yaml::Error),
    JSON(serde_json::Error),
    OpenSSL(openssl::error::ErrorStack),
    TryRecv(futures_channel::mpsc::TryRecvError),
    Send(futures_channel::mpsc::SendError),
    Decode,
    Encode,
    InvalidPacket,
    Authentication,
}

impl error::Error for Error {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        match self {
            Error::IO(err) => Some(err),
            Error::YAML(err) => Some(err),
            Error::JSON(err) => Some(err),
            Error::OpenSSL(err) => Some(err),
            Error::TryRecv(err) => Some(err),
            Error::Send(err) => Some(err),
            _ => None,
        }
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Error::IO(err) => write!(f, "I/O: {}", err),
            Error::YAML(err) => write!(f, "YAML: {}", err),
            Error::JSON(err) => write!(f, "JSON: {}", err),
            Error::OpenSSL(err) => write!(f, "OpenSSL: {}", err),
            Error::TryRecv(err) => write!(f, "TryRecv: {}", err),
            Error::Send(err) => write!(f, "Send: {}", err),
            Error::Decode => write!(f, "decoding"),
            Error::Encode => write!(f, "encoding"),
            Error::InvalidPacket => write!(f, "invalid packet"),
            Error::Authentication => write!(f, "authentication"),
        }
    }
}

impl From<io::Error> for Error {
    fn from(err: io::Error) -> Error {
        Error::IO(err)
    }
}

impl From<serde_yaml::Error> for Error {
    fn from(err: serde_yaml::Error) -> Error {
        Error::YAML(err)
    }
}

impl From<serde_json::Error> for Error {
    fn from(err: serde_json::Error) -> Error {
        Error::JSON(err)
    }
}

impl From<openssl::error::ErrorStack> for Error {
    fn from(err: openssl::error::ErrorStack) -> Error {
        Error::OpenSSL(err)
    }
}

impl From<futures_channel::mpsc::TryRecvError> for Error {
    fn from(err: futures_channel::mpsc::TryRecvError) -> Error {
        Error::TryRecv(err)
    }
}

impl From<futures_channel::mpsc::SendError> for Error {
    fn from(err: futures_channel::mpsc::SendError) -> Error {
        Error::Send(err)
    }
}
