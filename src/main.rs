#![feature(
    async_await,
    await_macro,
    futures_api,
    range_contains,
    const_slice_len,
    try_blocks
)]

mod chat;
mod config;
mod error;
mod network;
mod status;

use log::*;
use specs::prelude::*;

use crate::network::listener::{self, NetListener};

use std::thread;
use std::time::{Duration, Instant};

fn main() {
    env_logger::init();

    let config = match config::read_config() {
        Ok(cfg) => cfg,
        Err(err) => {
            error!("Loading config failed: {}", err);
            std::process::exit(1);
        }
    };

    let addr = config.net.address;
    let (net_tx, net_rx) = crossbeam_channel::unbounded();

    let net_listener = match NetListener::new(net_rx) {
        Ok(l) => l,
        Err(err) => {
            error!(
                "Failed to listen to address `{}`: {}",
                config.net.address, err
            );
            std::process::exit(2);
        }
    };

    let mut world = World::new();
    world.add_resource(config);

    thread::spawn(move || {
        let mut tick_dispatcher = DispatcherBuilder::new()
            .with(net_listener, "net_listener", &[])
            .with(status::StatusSystem, "status", &["net_listener"])
            .with(network::connection::NetHandler, "net_handler", &["status"])
            .build();
        tick_dispatcher.setup(&mut world.res);
        run_ticker(world, tick_dispatcher);
    });

    listener::listen(&addr, net_tx);
}

fn run_ticker(mut world: World, mut tick_dispatcher: Dispatcher) {
    let mut last_execution;
    let interval = Duration::from_millis(50);
    loop {
        last_execution = Instant::now();

        tick_dispatcher.dispatch(&world.res);
        world.maintain();

        let elapsed = last_execution.elapsed();
        if elapsed < interval {
            thread::sleep(interval - elapsed);
        }
    }
}
