use log::*;
use std::fmt;

use crate::network::packet::{ClientPacket, ServerPacket};
use futures_channel::mpsc;
use shrev::EventChannel;
use specs::prelude::*;
use std::sync::atomic::{AtomicBool, Ordering};

pub struct NetConnection {
    pub receiver: EventChannel<ServerPacket>,
    reader_rx: mpsc::UnboundedReceiver<ServerPacket>,
    sender: mpsc::UnboundedSender<ClientPacket>,
    closed: AtomicBool,
}

impl NetConnection {
    pub fn new(
        reader_rx: mpsc::UnboundedReceiver<ServerPacket>,
        sender: mpsc::UnboundedSender<ClientPacket>,
    ) -> NetConnection {
        NetConnection {
            receiver: EventChannel::new(),
            reader_rx,
            sender,
            closed: AtomicBool::from(false),
        }
    }

    pub fn is_closed(&self) -> bool {
        self.closed.load(Ordering::Relaxed)
    }

    pub fn set_closed(&self) {
        self.closed.store(true, Ordering::Relaxed);
    }

    pub fn send_packet(&self, packet: ClientPacket) {
        if let Err(err) = self.sender.unbounded_send(packet) {
            warn!("Failed to send packet to client: {}", err);
            self.set_closed();
        }
    }
}

impl Component for NetConnection {
    type Storage = DenseVecStorage<Self>;
}

pub struct NetHandler;

impl<'a> System<'a> for NetHandler {
    type SystemData = (Entities<'a>, WriteStorage<'a, NetConnection>);

    fn run(&mut self, (entities, mut connections): Self::SystemData) {
        for (entity, conn) in (&entities, &mut connections).join() {
            if !conn.is_closed() {
                loop {
                    let packet = match conn.reader_rx.try_next() {
                        Ok(Some(packet)) => packet,
                        Ok(None) => {
                            conn.set_closed();
                            break;
                        }
                        Err(_) => {
                            break;
                        }
                    };
                    conn.receiver.single_write(packet);
                }
            }

            if conn.is_closed() {
                debug!("Closing connection.");
                if let Err(err) = entities.delete(entity) {
                    warn!("Wrong generation while removing entity: {}", err);
                }
                continue;
            }
        }
    }

    fn setup(&mut self, res: &mut Resources) {
        Self::SystemData::setup(res);
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum State {
    Handshaking,
    Status,
    Login,
    Play,
}

impl Default for State {
    fn default() -> State {
        State::Handshaking
    }
}

impl fmt::Display for State {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            State::Handshaking => f.write_str("handshaking"),
            State::Status => f.write_str("status"),
            State::Login => f.write_str("login"),
            State::Play => f.write_str("play"),
        }
    }
}
