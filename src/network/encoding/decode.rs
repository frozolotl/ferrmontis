use crate::error::*;
use futures::prelude::*;
use log::*;
use std::ops::{Range, RangeBounds};

/// The maximum length a string in the protocol can have.
pub const MAX_STRING_SIZE: Range<usize> = 0..32768;

pub struct Decoder<R> {
    reader: R,
    remaining: usize,
}

impl<R> Decoder<R>
where
    R: AsyncRead,
{
    /// Creates new decoder without any limit
    pub fn new(reader: R) -> Decoder<R> {
        Decoder {
            reader,
            remaining: std::usize::MAX,
        }
    }

    pub fn with_limit(reader: R, limit: usize) -> Decoder<R> {
        Decoder {
            reader,
            remaining: limit,
        }
    }

    pub async fn read_bytes<'a>(&'a mut self, buf: &'a mut [u8]) -> Result<()> {
        if buf.len() <= self.remaining {
            await!(self.reader.read_exact(buf))?;
            self.remaining -= buf.len();
            Ok(())
        } else {
            Err(Error::Decode)
        }
    }

    pub async fn read_bool(&mut self) -> Result<bool> {
        let mut buf = [0];
        await!(self.read_bytes(&mut buf))?;
        match buf[0] {
            0 => Ok(false),
            1 => Ok(true),
            _ => {
                debug!("attempted to decode non 0 or 1 boolean");
                Err(Error::Decode)
            }
        }
    }

    pub async fn read_u8(&mut self) -> Result<u8> {
        let mut buf = [0];
        await!(self.read_bytes(&mut buf))?;
        Ok(buf[0])
    }

    pub async fn read_i8(&mut self) -> Result<i8> {
        Ok(await!(self.read_u8())? as i8)
    }

    pub async fn read_u16(&mut self) -> Result<u16> {
        let mut buf = [0; 2];
        await!(self.read_bytes(&mut buf))?;
        Ok(u16::from_be_bytes(buf))
    }

    pub async fn read_i16(&mut self) -> Result<i16> {
        Ok(await!(self.read_u16())? as i16)
    }

    pub async fn read_u32(&mut self) -> Result<u32> {
        let mut buf = [0; 4];
        await!(self.read_bytes(&mut buf))?;
        Ok(u32::from_be_bytes(buf))
    }

    pub async fn read_i32(&mut self) -> Result<i32> {
        Ok(await!(self.read_u32())? as i32)
    }

    pub async fn read_u64(&mut self) -> Result<u64> {
        let mut buf = [0; 8];
        await!(self.read_bytes(&mut buf))?;
        Ok(u64::from_be_bytes(buf))
    }

    pub async fn read_i64(&mut self) -> Result<i64> {
        Ok(await!(self.read_u64())? as i64)
    }

    pub async fn read_f32(&mut self) -> Result<f32> {
        Ok(f32::from_bits(await!(self.read_u32())?))
    }

    pub async fn read_f64(&mut self) -> Result<f64> {
        Ok(f64::from_bits(await!(self.read_u64())?))
    }

    pub async fn read_var_i32_bytes_read(&mut self) -> Result<(i32, usize)> {
        let mut v = 0;
        let mut i = 0;

        loop {
            let b = await!(self.read_u8())?;
            let c = i32::from(b & 0b0111_1111);
            v |= c << (7 * i);

            i += 1;
            if i > 5 {
                return Err(Error::Decode);
            }
            if b & 0b1000_0000 == 0 {
                break;
            }
        }

        Ok((v, i))
    }

    pub async fn read_var_i32(&mut self) -> Result<i32> {
        Ok(await!(self.read_var_i32_bytes_read())?.0)
    }

    pub async fn read_var_i64(&mut self) -> Result<i64> {
        let mut v = 0;
        let mut i = 0;

        loop {
            let b = await!(self.read_u8())?;
            let c = i64::from(b & 0b0111_1111);
            v |= c << (7 * i);

            i += 1;
            if i > 10 {
                return Err(Error::Decode);
            }
            if b & 0b1000_0000 == 0 {
                break;
            }
        }

        Ok(v)
    }

    /// Decodes a string with a length being constrained by `len_range`.
    pub async fn read_string<'a, B>(&'a mut self, len_range: B) -> Result<String>
    where
        B: RangeBounds<usize> + 'a,
    {
        let length = await!(self.read_var_i32())?;
        if length < 0 {
            warn!("string has negative length");
            return Err(Error::Decode);
        }

        let length = length as usize;
        if !len_range.contains(&length) {
            warn!("string is not in range");
            return Err(Error::Decode);
        }

        let mut buf = vec![0; length];
        await!(self.read_bytes(&mut buf))?;

        String::from_utf8(buf).map_err(|_| Error::Decode)
    }
}
