use crate::error::*;
use futures::prelude::*;
use log::*;

pub struct Encoder<W> {
    writer: W,
}

impl<W> Encoder<W>
where
    W: AsyncWrite,
{
    pub fn new(writer: W) -> Encoder<W> {
        Encoder { writer }
    }

    pub async fn write_bytes<'a>(&'a mut self, bytes: &'a [u8]) -> Result<()> {
        if !bytes.is_empty() {
            await!(self.writer.write_all(bytes))?;
        }
        Ok(())
    }

    pub async fn write_bool(&mut self, b: bool) -> Result<()> {
        let buf = [b as u8];
        await!(self.writer.write_all(&buf))?;
        Ok(())
    }

    pub async fn write_u8(&mut self, u: u8) -> Result<()> {
        let buf = [u];
        await!(self.writer.write_all(&buf))?;
        Ok(())
    }

    pub async fn write_i8(&mut self, i: i8) -> Result<()> {
        await!(self.write_u8(i as u8))
    }

    pub async fn write_u16(&mut self, u: u16) -> Result<()> {
        let buf = u.to_be_bytes();
        await!(self.writer.write_all(&buf))?;
        Ok(())
    }

    pub async fn write_i16(&mut self, i: i16) -> Result<()> {
        await!(self.write_u16(i as u16))
    }

    pub async fn write_u32(&mut self, u: u32) -> Result<()> {
        let buf = u.to_be_bytes();
        await!(self.writer.write_all(&buf))?;
        Ok(())
    }

    pub async fn write_i32(&mut self, i: i32) -> Result<()> {
        await!(self.write_u32(i as u32))
    }

    pub async fn write_u64(&mut self, u: u64) -> Result<()> {
        let buf = u.to_be_bytes();
        await!(self.writer.write_all(&buf))?;
        Ok(())
    }

    pub async fn write_i64(&mut self, i: i64) -> Result<()> {
        await!(self.write_u64(i as u64))
    }

    pub async fn write_f32(&mut self, f: f32) -> Result<()> {
        let u = f.to_bits();
        await!(self.write_u32(u))
    }

    pub async fn write_f64(&mut self, f: f64) -> Result<()> {
        let u = f.to_bits();
        await!(self.write_u64(u))
    }

    pub async fn write_var_i32(&mut self, i: i32) -> Result<()> {
        let mut u = i as u32;

        let mut buf = [0; 5];
        let mut written = 0;

        loop {
            buf[written] = (u & 0b0111_1111) as u8;
            u >>= 7;

            if u == 0 {
                break;
            }

            buf[written] |= 0b1000_0000;
            written += 1;
        }

        await!(self.write_bytes(&buf[..=written]))
    }

    pub async fn write_var_i64(&mut self, i: i64) -> Result<()> {
        let mut u = i as u64;

        let mut buf = [0; 10];
        let mut written = 0;

        loop {
            buf[written] = (u & 0b0111_1111) as u8;
            u >>= 7;

            if u == 0 {
                break;
            }

            buf[written] |= 0b1000_0000;
            written += 1;
        }

        await!(self.write_bytes(&buf[..=written]))
    }

    /// Writes a string with a length less than 32768.
    pub async fn write_string<'a>(&'a mut self, s: &'a str) -> Result<()> {
        if s.len() >= 32768 {
            warn!("tried to write too long string");
            return Err(Error::Encode);
        }

        await!(self.write_var_i32(s.len() as i32))?;
        await!(self.write_bytes(s.as_bytes()))?;

        Ok(())
    }
}

/// Calculates the length of certain operations
#[derive(Copy, Clone)]
pub struct LengthCalculator {
    length: usize,
}

impl LengthCalculator {
    pub const fn new() -> LengthCalculator {
        LengthCalculator { length: 0 }
    }

    pub const fn len(self) -> usize {
        self.length
    }

    pub const fn write_bytes(self, b: &[u8]) -> LengthCalculator {
        LengthCalculator {
            length: self.length + b.len(),
        }
    }

    pub const fn write_bool(self) -> LengthCalculator {
        LengthCalculator {
            length: self.length + 1,
        }
    }

    pub const fn write_u8(self) -> LengthCalculator {
        LengthCalculator {
            length: self.length + 1,
        }
    }

    pub const fn write_i8(self) -> LengthCalculator {
        self.write_u8()
    }

    pub const fn write_u16(self) -> LengthCalculator {
        LengthCalculator {
            length: self.length + 2,
        }
    }

    pub const fn write_i16(self) -> LengthCalculator {
        self.write_u16()
    }

    pub const fn write_u32(self) -> LengthCalculator {
        LengthCalculator {
            length: self.length + 4,
        }
    }

    pub const fn write_i32(self) -> LengthCalculator {
        self.write_u32()
    }

    pub const fn write_u64(self) -> LengthCalculator {
        LengthCalculator {
            length: self.length + 8,
        }
    }

    pub const fn write_i64(self) -> LengthCalculator {
        self.write_u64()
    }

    pub fn write_var_i32(mut self, i: i32) -> LengthCalculator {
        let mut u = i as u32;

        loop {
            u >>= 7;

            self.length += 1;
            if u == 0 {
                break;
            }
        }

        self
    }

    pub fn write_var_i64(mut self, i: i64) -> LengthCalculator {
        let mut u = i as u32;

        loop {
            u >>= 7;

            self.length += 1;
            if u == 0 {
                break;
            }
        }

        self
    }

    /// Writes a string with a length less than 32768.
    pub fn write_string(mut self, s: &str) -> LengthCalculator {
        if s.len() >= 32768 {
            warn!("tried to write too long string");
        }

        self = self.write_var_i32(s.len() as i32);
        self.length += s.len();
        self
    }
}
