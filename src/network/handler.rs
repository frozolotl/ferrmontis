use crate::error::*;
use log::*;

use crate::network::{
    connection::State,
    packet::{
        handshake::Handshake,
        login::{ClientLogin, ServerLogin},
        status::{ClientStatus, ServerStatus},
        ClientPacket, ServerPacket,
    },
};
use futures::{executor::ThreadPool, prelude::*, task::SpawnExt};
use futures_channel::mpsc;
use openssl::rsa::{self, Rsa};
use romio::TcpStream;

pub async fn handle_connection(
    mut connection: TcpStream,
    packet_rx: mpsc::UnboundedSender<ServerPacket>,
    packet_tx: mpsc::UnboundedReceiver<ClientPacket>,
    mut threadpool: ThreadPool,
) {
    let packet = await!(ServerPacket::decode_from(
        &mut connection,
        State::Handshaking
    ));
    let state = match packet {
        Ok(ServerPacket::Handshaking(Handshake { state, .. })) => state,
        Ok(_) => unreachable!(),
        Err(err) => {
            warn!("Decoding packet failed: {}", err);
            return;
        }
    };

    let res = match state {
        State::Status => await!(handle_status(connection, packet_rx, packet_tx)),
        State::Login => match await!(handle_login(&mut connection)) {
            Ok(shared_secret) => {
                let (read, write) = connection.split();
                let res = threadpool
                    .spawn(handle_play_send(write, packet_tx, shared_secret))
                    .and_then(|_| {
                        threadpool.spawn(handle_play_receive(read, packet_rx, shared_secret))
                    });
                if let Err(err) = res {
                    warn!("Failed to spawn futures: {:?}", err);
                }

                return;
            }
            Err(err) => Err(err),
        },
        _ => unreachable!(),
    };
    if let Err(err) = res {
        warn!("Failed to handle connection: {}", err);
    }
}

async fn handle_status(
    mut connection: impl AsyncRead + AsyncWrite,
    packet_rx: mpsc::UnboundedSender<ServerPacket>,
    mut packet_tx: mpsc::UnboundedReceiver<ClientPacket>,
) -> Result<()> {
    // request
    let packet = await!(ServerPacket::decode_from(&mut connection, State::Status));
    match packet? {
        packet @ ServerPacket::Status(ServerStatus::Request) => packet_rx
            .unbounded_send(packet)
            .map_err(mpsc::TrySendError::into_send_error)?,
        _ => return Err(Error::InvalidPacket),
    }
    // response
    match await!(packet_tx.next()) {
        Some(to_send) => await!(to_send.encode(&mut connection))?,
        None => return Ok(()),
    }

    // ping
    let packet = await!(ServerPacket::decode_from(&mut connection, State::Status));
    // pong
    match packet? {
        ServerPacket::Status(ServerStatus::Ping(u)) => {
            let to_send = ClientPacket::Status(ClientStatus::Pong(u));
            await!(to_send.encode(&mut connection))?;

            Ok(())
        }
        _ => Err(Error::InvalidPacket),
    }
}

async fn handle_login(mut connection: impl AsyncRead + AsyncWrite) -> Result<Option<[u8; 16]>> {
    // start login
    let packet = await!(ServerPacket::decode_from(&mut connection, State::Login));
    let username = match packet? {
        ServerPacket::Login(ServerLogin::StartLogin(username)) => username,
        _ => return Err(Error::InvalidPacket),
    };

    info!("{} tries to join server.", username);

    if false {
        // encryption request
        let rsa = Rsa::generate(1024)?;
        let public_key = rsa.public_key_to_der()?;
        let mut verify_token = [0; 16];
        openssl::rand::rand_bytes(&mut verify_token)?;

        let to_send = ClientPacket::Login(ClientLogin::EncryptionRequest {
            public_key,
            verify_token: verify_token.to_vec(),
        });
        await!(to_send.encode(&mut connection))?;

        // encryption response
        let packet = await!(ServerPacket::decode_from(&mut connection, State::Login));
        #[allow(unused)]
        let shared_secret = match packet? {
            ServerPacket::Login(ServerLogin::EncryptionResponse { secret, token }) => {
                let mut shared_secret = [0; 128];
                let secret_len =
                    rsa.private_decrypt(&secret, &mut shared_secret, rsa::Padding::PKCS1)?;

                if secret_len != 16 {
                    warn!("Got non-standard secret length");
                    return Err(Error::Authentication);
                }

                let mut check_token = [0; 128];
                let token_length =
                    rsa.private_decrypt(&token, &mut check_token, rsa::Padding::PKCS1)?;

                if check_token[..token_length] != verify_token {
                    warn!(
                        "Check token is not the same as own token: {:x?} != {:x?}",
                        &check_token[..],
                        token
                    );
                    return Err(Error::Authentication);
                }

                let mut secret = [0; 16];
                secret.copy_from_slice(&shared_secret[..16]);
                secret
            }
            _ => return Err(Error::InvalidPacket),
        };
    }

    // login success
    let mut uuid_bytes = [0; 16];
    openssl::rand::rand_bytes(&mut uuid_bytes)?;
    let uuid = uuid::Uuid::from_bytes(uuid_bytes)
        .to_hyphenated()
        .to_string();
    let to_send = ClientPacket::Login(ClientLogin::LoginSuccess { uuid, username });
    await!(to_send.encode(&mut connection))?;

    Ok(None)
}

async fn handle_play_send(
    mut connection: impl AsyncWrite,
    mut packet_tx: mpsc::UnboundedReceiver<ClientPacket>,
    _shared_secret: Option<[u8; 16]>,
) {
    while let Some(packet) = await!(packet_tx.next()) {
        if let Err(err) = await!(packet.encode(&mut connection)) {
            warn!("Could not encode packet: {}", err);
            return;
        }
    }
}

async fn handle_play_receive(
    mut connection: impl AsyncRead,
    mut packet_rx: mpsc::UnboundedSender<ServerPacket>,
    _shared_secret: Option<[u8; 16]>,
) {
    loop {
        match await!(ServerPacket::decode_from(&mut connection, State::Play)) {
            Ok(packet) => {
                if let Err(err) = await!(packet_rx.send(packet)) {
                    warn!("Could not send packet to channel: {}", err);
                    return;
                }
            }
            Err(err) => {
                warn!("Could not decode packet: {}", err);
                return;
            }
        };
    }
}
