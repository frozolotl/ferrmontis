use crate::error::*;
use log::*;

use crate::network::{connection::NetConnection, handler};
use crossbeam_channel::{Receiver, Sender};
use futures::{
    executor::{self, ThreadPool},
    stream::StreamExt,
    task::SpawnExt,
};
use futures_channel::mpsc;
use romio::tcp::TcpListener;
use specs::prelude::{Read, *};
use std::net::SocketAddr;

pub fn listen(address: &SocketAddr, tx: Sender<NetConnection>) {
    let mut listener = match TcpListener::bind(address) {
        Ok(l) => l,
        Err(err) => {
            error!("Failed to listen at `{}`: {}", address, err);
            std::process::exit(2);
        }
    };

    executor::block_on(
        async move {
            let mut threadpool = match ThreadPool::new() {
                Ok(tp) => tp,
                Err(err) => {
                    error!("Could not initialize threadpool: {}", err);
                    std::process::exit(3);
                }
            };
            let mut incoming = listener.incoming();
            while let Some(conn) = await!(incoming.next()) {
                let conn = match conn {
                    Ok(conn) => conn,
                    Err(err) => {
                        warn!("Could not open connection: {}", err);
                        continue;
                    }
                };
                match conn.peer_addr() {
                    Ok(addr) => {
                        debug!("Connection `{}` opened.", addr);
                    }
                    Err(_) => {
                        debug!("Connection opened.");
                    }
                }

                let (reader_tx, reader_rx) = mpsc::unbounded();
                let (writer_tx, writer_rx) = mpsc::unbounded();

                let res = threadpool.spawn(handler::handle_connection(conn, reader_tx, writer_rx, threadpool.clone()));

                if let Err(err) = res {
                    warn!("Could not handle connection: {:?}", err);
                    continue;
                }

                let net_connection = NetConnection::new(reader_rx, writer_tx);

                if let Err(err) = tx.send(net_connection) {
                    warn!("Could not send stream to handler: {}", err);
                }
            }
        },
    );
}

pub struct NetListener {
    rx: Receiver<NetConnection>,
}

impl NetListener {
    pub fn new(rx: Receiver<NetConnection>) -> Result<NetListener> {
        Ok(NetListener { rx })
    }
}

impl<'a> System<'a> for NetListener {
    type SystemData = (Entities<'a>, Read<'a, LazyUpdate>);

    fn run(&mut self, (entities, updater): Self::SystemData) {
        for conn in self.rx.try_iter() {
            let player = entities.create();
            updater.insert(player, conn);
        }
    }
}
