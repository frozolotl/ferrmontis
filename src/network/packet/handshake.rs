use crate::error::*;
use crate::network::connection::State;
use crate::network::encoding::Decoder;
use futures::prelude::*;
use log::*;

/// A serverbound packet which specifies the state to switch to.
#[derive(Debug)]
pub struct Handshake {
    pub protocol_version: i32,
    pub address: String,
    pub port: u16,
    pub state: State,
}

impl Handshake {
    pub async fn decode_from(_id: i32, mut decoder: Decoder<impl AsyncRead>) -> Result<Handshake> {
        let protocol_version = await!(decoder.read_var_i32())?;
        let address = await!(decoder.read_string(0..256))?;
        let port = await!(decoder.read_u16())?;

        let state = await!(decoder.read_var_i32())?;
        let state = match state {
            1 => State::Status,
            2 => State::Login,
            _ => {
                debug!("tried to decode invalid handshake target state");
                return Err(Error::Decode);
            }
        };

        Ok(Handshake {
            protocol_version,
            address,
            port,
            state,
        })
    }

    pub fn is_valid_id(id: i32) -> bool {
        id == 0
    }
}
