use crate::chat::Chat;
use crate::error::*;
use crate::network::encoding::*;
use futures::prelude::*;
use log::*;

/// Serverbound login packets
#[derive(Debug, Eq, PartialEq)]
pub enum ServerLogin {
    /// Start logging in with a specific username.
    StartLogin(String),
    /// Some encryption stuff.
    EncryptionResponse { secret: Vec<u8>, token: Vec<u8> },
}

impl ServerLogin {
    pub async fn decode_from(id: i32, mut decoder: Decoder<impl AsyncRead>) -> Result<ServerLogin> {
        match id {
            0x00 => {
                let username = await!(decoder.read_string(1..=16))?;
                Ok(ServerLogin::StartLogin(username))
            }
            0x01 => {
                let secret_length = await!(decoder.read_var_i32())? as usize;
                if secret_length != 128 {
                    warn!("Secret is {} instead of 128 bytes long.", secret_length);
                    return Err(Error::Decode);
                }
                let mut secret = vec![0; 128];
                await!(decoder.read_bytes(&mut secret))?;

                let token_length = await!(decoder.read_var_i32())? as usize;
                if token_length != 128 {
                    warn!("Secret is {} instead of 128 bytes long.", secret_length);
                    return Err(Error::Decode);
                }

                let mut token = vec![0; 128];
                await!(decoder.read_bytes(&mut token))?;

                Ok(ServerLogin::EncryptionResponse { secret, token })
            }
            _ => unreachable!(),
        }
    }

    pub fn is_valid_id(id: i32) -> bool {
        id == 0 || id == 1
    }
}

/// Clientbound login packets
#[derive(Debug)]
pub enum ClientLogin {
    Disconnect(Box<Chat>),
    EncryptionRequest {
        public_key: Vec<u8>,
        verify_token: Vec<u8>,
    },
    LoginSuccess {
        uuid: String,
        username: String,
    },
}

impl ClientLogin {
    pub async fn encode<'a>(&'a self, mut encoder: Encoder<impl AsyncWrite + 'a>) -> Result<()> {
        match self {
            ClientLogin::Disconnect(chat) => {
                let content = serde_json::to_string(&chat)?;
                let length = LengthCalculator::new()
                    .write_var_i32(0) // packet id
                    .write_string(&content)
                    .len();

                await!(encoder.write_var_i32(length as i32))?;
                await!(encoder.write_var_i32(0))?; // packet id
                await!(encoder.write_string(&content))?;
            }
            ClientLogin::EncryptionRequest {
                public_key,
                verify_token,
            } => {
                let length = LengthCalculator::new()
                    .write_var_i32(1) // the packet id
                    .write_var_i32(0) // the server id, which is always empty
                    .write_var_i32(public_key.len() as i32)
                    .write_bytes(public_key)
                    .write_var_i32(verify_token.len() as i32)
                    .write_bytes(verify_token)
                    .len();

                await!(encoder.write_var_i32(length as i32))?;
                await!(encoder.write_var_i32(1))?; // packet id
                await!(encoder.write_var_i32(0))?; // server id
                await!(encoder.write_var_i32(public_key.len() as i32))?;
                await!(encoder.write_bytes(public_key))?;
                await!(encoder.write_var_i32(verify_token.len() as i32))?;
                await!(encoder.write_bytes(verify_token))?;
            }
            ClientLogin::LoginSuccess { uuid, username } => {
                let length = LengthCalculator::new()
                    .write_var_i32(2) // packet id
                    .write_string(&uuid)
                    .write_string(&username)
                    .len();

                await!(encoder.write_var_i32(length as i32))?;
                await!(encoder.write_var_i32(2))?; // packet id
                await!(encoder.write_string(&uuid))?;
                await!(encoder.write_string(&username))?;
            }
        }

        Ok(())
    }
}
