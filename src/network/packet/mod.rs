pub mod handshake;
pub mod login;
pub mod status;

use crate::error::*;
use crate::network::connection::State;
use crate::network::encoding::{Decoder, Encoder};
use futures::prelude::*;
use log::*;

/// All serverbound packets.
#[derive(Debug)]
pub enum ServerPacket {
    Handshaking(handshake::Handshake),
    Status(status::ServerStatus),
    Login(login::ServerLogin),
}

impl ServerPacket {
    pub async fn decode_from(mut reader: impl AsyncRead, state: State) -> Result<ServerPacket> {
        let mut decoder = Decoder::new(&mut reader);
        let length = await!(decoder.read_var_i32())?;
        if length <= 0 {
            warn!("Client sent packet with negative or zero length {}", length);
            return Err(Error::Decode);
        }

        let (id, read) = await!(decoder.read_var_i32_bytes_read())?;

        let length = length as usize - read;
        let decoder = Decoder::with_limit(reader, length);

        let packet = match state {
            State::Handshaking if handshake::Handshake::is_valid_id(id) => {
                let decoded = await!(handshake::Handshake::decode_from(id, decoder))?;
                ServerPacket::Handshaking(decoded)
            }
            State::Status if status::ServerStatus::is_valid_id(id) => {
                let decoded = await!(status::ServerStatus::decode_from(id, decoder))?;
                ServerPacket::Status(decoded)
            }
            State::Login if login::ServerLogin::is_valid_id(id) => {
                let decoded = await!(login::ServerLogin::decode_from(id, decoder))?;
                ServerPacket::Login(decoded)
            }
            _ => {
                warn!("Client used unknown packet id {:x} in state {}", id, state);
                return Err(Error::Decode);
            }
        };

        trace!("Decoded packet: {:?}", packet);
        Ok(packet)
    }
}

/// All clientbound packets.
#[derive(Debug)]
pub enum ClientPacket {
    Status(status::ClientStatus),
    Login(login::ClientLogin),
}

impl ClientPacket {
    pub async fn encode<'a>(&'a self, writer: impl AsyncWrite + 'a) -> Result<()> {
        trace!("Encoding packet: {:?}", self);
        let encoder = Encoder::new(writer);
        match self {
            ClientPacket::Status(status) => await!(status.encode(encoder)),
            ClientPacket::Login(login) => await!(login.encode(encoder)),
        }
    }
}
