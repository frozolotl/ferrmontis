use crate::error::*;
use crate::network::encoding::*;
use futures::prelude::*;

/// Serverbound status packets
#[derive(Debug, Eq, PartialEq)]
pub enum ServerStatus {
    /// A packet that indicates that the server wants some status information.
    Request,
    /// A packet containing the unix timestamp of the client machine.
    Ping(i64),
}

impl ServerStatus {
    pub async fn decode_from(
        id: i32,
        mut decoder: Decoder<impl AsyncRead>,
    ) -> Result<ServerStatus> {
        match id {
            0 => Ok(ServerStatus::Request),
            1 => {
                let ping = await!(decoder.read_i64())?;
                Ok(ServerStatus::Ping(ping))
            }
            _ => unreachable!(),
        }
    }

    pub fn is_valid_id(id: i32) -> bool {
        id == 0 || id == 1
    }
}

/// Clientbound status packets
#[derive(Debug)]
pub enum ClientStatus {
    /// A packet returning some information about the server.
    Response(String),
    /// A packet containing the unix timestamp of the client machine.
    Pong(i64),
}

impl ClientStatus {
    pub async fn encode<'a>(&'a self, mut encoder: Encoder<impl AsyncWrite + 'a>) -> Result<()> {
        match self {
            ClientStatus::Response(s) => {
                let length = LengthCalculator::new()
                    .write_var_i32(0)
                    .write_string(s)
                    .len();

                await!(encoder.write_var_i32(length as i32))?;
                await!(encoder.write_var_i32(0))?;
                await!(encoder.write_string(s))?;
            }
            ClientStatus::Pong(v) => {
                let length = LengthCalculator::new().write_var_i32(1).write_i64().len();

                await!(encoder.write_var_i32(length as i32))?;
                await!(encoder.write_var_i32(1))?;
                await!(encoder.write_i64(*v))?;
            }
        }

        Ok(())
    }
}
