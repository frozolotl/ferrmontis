use crate::config::Config;
use crate::network::{
    connection::NetConnection,
    packet::{
        status::{ClientStatus, ServerStatus},
        ClientPacket, ServerPacket,
    },
};
use shrev::ReaderId;
use specs::prelude::*;

use serde::Serialize;

pub struct StatusSystem;

impl<'a> System<'a> for StatusSystem {
    type SystemData = (
        Entities<'a>,
        WriteStorage<'a, NetConnection>,
        WriteStorage<'a, StatusReader>,
        Read<'a, LazyUpdate>,
        Read<'a, Config>,
    );

    fn run(
        &mut self,
        (entities, mut connections, mut status_reader, updater, config): Self::SystemData,
    ) {
        for (entity, conn, ()) in (&entities, &mut connections, !&status_reader).join() {
            updater.insert(
                entity,
                StatusReader {
                    reader: conn.receiver.register_reader(),
                },
            );
        }

        let cfg = &config.status;
        for (conn, reader) in (&connections, &mut status_reader).join() {
            for packet in conn.receiver.read(&mut reader.reader) {
                if let ServerPacket::Status(ServerStatus::Request) = packet {
                    let favicon = match &cfg.favicon {
                        Some(s) => Some(s.as_str()),
                        None => None,
                    };
                    let resp = Response {
                        version: Version {
                            name: "1.12.2",
                            protocol: 340,
                        },
                        players: Players {
                            max: cfg.max_players,
                            online: 10,
                            sample: vec![Player {
                                name: "frozolotl",
                                id: "c0373c16-0645-4706-bc9b-59565990685d",
                            }],
                        },
                        description: MinChat {
                            text: &cfg.description,
                        },
                        favicon,
                    };

                    let resp = serde_json::to_string(&resp).expect("could not serialize response");
                    conn.send_packet(ClientPacket::Status(ClientStatus::Response(resp)));
                }
            }
        }
    }

    fn setup(&mut self, res: &mut Resources) {
        Self::SystemData::setup(res);
    }
}

pub struct StatusReader {
    reader: ReaderId<ServerPacket>,
}

impl Component for StatusReader {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Serialize)]
struct Response<'a> {
    version: Version<'a>,
    players: Players<'a>,
    description: MinChat<'a>,

    #[serde(skip_serializing_if = "Option::is_none")]
    favicon: Option<&'a str>,
}

#[derive(Serialize)]
struct Version<'a> {
    name: &'a str,
    protocol: i32,
}

#[derive(Serialize)]
struct Players<'a> {
    max: u32,
    online: u32,
    sample: Vec<Player<'a>>,
}

#[derive(Serialize)]
struct Player<'a> {
    name: &'a str,
    id: &'a str,
}

/// Minimal chat, since the client can only use `text`
#[derive(Serialize)]
struct MinChat<'a> {
    text: &'a str,
}
